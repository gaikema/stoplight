/*
	Circle.ts
*/

export class Circle
{
	public pt: [number, number];
	public radius: number;
	public color: string;

	private ctx: CanvasRenderingContext2D;

	/**
	 * Creates an instance of Circle.
	 * 
	 * @param {[number, number]} pt Center of the circle.
	 * @param {number} radius Radius of the circle.
	 * @param {string} color Color of the circle.
	 * @param {CanvasRenderingContext2D} ctx
	 * 
	 * @memberOf Circle
	 */
	constructor(pt: [number, number], radius: number, color: string, ctx: CanvasRenderingContext2D)
	{
		this.pt = pt;
		this.radius = radius;
		this.ctx = ctx;
		this.color = color

		this.draw();
	}

	/**
	 * Changes the color of the circle and redraws it.
	 * 
	 * @param {string} color
	 * 
	 * @memberOf Circle
	 */
	public change_color(color: string): void
	{
		this.color = color;
		this.draw();
	}

	/**
	 * Draws the circle using the context.
	 * 
	 * @private
	 * 
	 * @memberOf Circle
	 */
	private draw(): void
	{
		this.ctx.beginPath();
		this.ctx.arc(this.pt[0], this.pt[1], this.radius, 0, 2*Math.PI);

		// http://www.html5canvastutorials.com/tutorials/html5-canvas-circles/
		this.ctx.fillStyle = this.color;
		this.ctx.strokeStyle = "white";
      	this.ctx.fill();

		this.ctx.stroke();
	}
}