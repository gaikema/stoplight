/*
	main.ts
*/

import {Circle} from "./Circle"

let canvas: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("myCanvas");
let ctx: CanvasRenderingContext2D = canvas.getContext('2d');

// x-coordinate for each light.
let x = 375/2;
// y-coordinate for the first light; used to calculate the coordinate of the other lights.
let y = 200;
// Radius for each light.
let r = 110;

// The y-coordinate of the nth circle is given by 1.1n*y.
let red: Circle = new Circle([x, y], 110, "red", ctx);
// Have the other 2 start off black, so that the light is initially red.
let yellow: Circle = new Circle([x, 2.1*y], r, "black", ctx);
let green: Circle = new Circle([x, 3.2*y], r, "black", ctx);

// The correct order is: red, green, yellow.
let i: number = 0;
setInterval(function(){
	if (i % 3 == 0)
	{
		// Change to red.
		red.change_color("red");
		yellow.change_color("black");
		green.change_color("black");
	}
	else if (i % 3 == 1)
	{
		// Change to green.
		red.change_color("black");
		yellow.change_color("black");
		green.change_color("green");

	}
	else if (i % 3 == 2)
	{
		// Change to yellow.
		red.change_color("black");
		yellow.change_color("yellow");
		green.change_color("black");
	}

	i++;
	console.log(i);
}, 3*1000);